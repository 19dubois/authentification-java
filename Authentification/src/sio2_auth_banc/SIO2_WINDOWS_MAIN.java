/*
 * Copyright (C) 2020 pfitz@ac-reims.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sio2_auth_banc;

/**
 * BTS SIO2 - SLAM - JAVA
 *
 * ---------------------------
 *
 * Liaison entre deux fenêtres, alternativement visibles
 *
 * @author pfitz@ac-reims.fr
 * Modifié par Marcellin DUBOIS <marcellin.dubois@orange.fr>
 */
public class SIO2_WINDOWS_MAIN
    {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        {
        FEN_UNE f1 = new FEN_UNE();
        FEN_DEUX f2 = new FEN_DEUX();
        f1.setFenDeux(f2);
        f2.setFenUn(f1);
        f1.pack();
        f2.pack();
        f1.setVisible(true);
        }
    }
