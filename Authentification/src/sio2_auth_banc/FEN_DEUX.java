/*
 * Copyright (C) 2016 pfitz@ac-reims.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sio2_auth_banc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JLabel;

/**
 *
 * Class FEN_DEUX
 * 
 * Récupère et affiche les informations à propos du compte de l'utilisateur
 * 
 * @author pfitz@ac-reims.fr
 * Modifié par Marcellin DUBOIS <marcellin.dubois@orange.fr>
 */
public class FEN_DEUX extends JFrame
    {
    private FEN_UNE fenetreUn;
    private JPanel panneau;
    private JTextField zoneText;
    private JButton setCadreUn;
    private JLabel titre,nom,prenom,compte,barre;
    private String barreVar = "--------------------------------------------------------------------------------------";

    /**
     * Constructeur
     * 
     * Réalise l'affichage de la fenêtre FEN_DEUX
     * 
     * @throws HeadlessException
     */
    public FEN_DEUX() throws HeadlessException
        {
        initComposant();
        }

    /** 
     * initComposant
     * 
     * Réalise l'affichage de la fenêtre FEN_DEUX
     * 
     * return: void
    */
    private void initComposant()
        {
        setLocation(600, 200);
        setTitle("Agence Ard€nnes Compte");
        setSize(600, 600);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Border border = BorderFactory.createLineBorder(Color.blue);
        panneau = new JPanel(new GridLayout(0, 1, 10, 5));
        panneau.setBorder(new EmptyBorder(10, 10, 10, 10));
        
        titre = new JLabel("<html><b>Agence Ard€nnes</b><html>");
        titre.setFont(new Font(Font.DIALOG, Font.PLAIN, 48));
        nom = new JLabel("<html><b>Nom: </b><html>");
        nom.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
        prenom = new JLabel("<html><b>Prénom: </b><html>");
        prenom.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
        barre = new JLabel(barreVar);
        barre.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
        compte = new JLabel("<html><b>Compte: </b><br><br><b>Solde: </b><br><br><html>");
        compte.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));

        setCadreUn = new JButton("<html><b>Déconnexion</b><html>");
        setCadreUn.addActionListener(new ActionListener()
            {
            @Override
            public void actionPerformed(ActionEvent e)
                {
                setNom("<html><b>Nom:</b><html>");
                setPrenom("<html><b>Prenom:</b><html>");
                fenetreUn.setVisible(true);
                FEN_DEUX.this.setVisible(false);
                //fenetreUn.setText(FEN_DEUX.this.getText());
                }
            });
        setCadreUn.setBorder(border);
        
        panneau.add(titre);
        panneau.add(nom);
        panneau.add(prenom);
        panneau.add(barre);
        panneau.add(compte);
        panneau.add(setCadreUn);

        super.add(panneau, BorderLayout.CENTER);
        }

    /**
     * setFenUn
     * 
     * Assigne la FEN_UN à la variable global fenetreUn
     * 
     * @param f1
     */
    public void setFenUn(FEN_UNE f1)
        {
        this.fenetreUn = f1;
        }
    /**
     * setNom
     * 
     * Assigne le nom du client au labbel nom
     * 
     * @param nom : String
     */
    public void setNom(String nom){
        this.nom.setText("<html><b>Nom: "+ nom +"</b><html>");
    }
    /**
     * setPrenom
     * 
     * Assigne le prenom du client au labbel prenom
     * 
     * @param prenom : String
     */
    public void setPrenom(String prenom){
        this.prenom.setText("<html><b>Prenom: "+ prenom +"</b><html>");
    }
    /**
     * setCompte
     * 
     * Assigne le montant et le numéro du compte au labbel compte
     * 
     * @param compte :String[]
     * @param solde :String[]
     */
    public void setCompte(String[] compte, String[] solde){
        int size =compte.length;
        String result = "";
        for (int i = 0; i < size; i++){
            if(compte[i]!=null){
                result += "<b>Compte: "+ compte[i] +"</b><br><b>Solde: "+ solde[i] +" €</b><br>";
            }
        }
        this.compte.setText("<html>"+ result + "<html>");
    }
}
