/*
 * Copyright (C) 2020 pfitz@ac-reims.fr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sio2_auth_banc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;

/**
 *
 * @author pfitz@ac-reims.fr
 * Modifié par Marcellin DUBOIS <marcellin.dubois@orange.fr>
 */

public class FEN_UNE extends JFrame
    {
    // Référence future à la deuxième fenêtre
    private FEN_DEUX fenetreDeux;
    // panel par déf.
    private JPanel panneau;
    // Une zone texte
    private JTextField zoneText,zoneTextMdp;
    private JButton setCadreDeux;
    private JLabel titre,nom,mdp;
    /**
     * FEN_UNE
     * 
     * Réalise l'affichage de la fenêtre FEN_UN
     */
    public FEN_UNE()
        {
        this.initComposant();
        }
    
    /**
     * initComposant
     * 
     * Réalise l'affichage de la fenêtre FEN_UN en plus de récupérer et d'envoyer des requêtes et des informations à la class BDD & FEN_DEUX
     */
    private void initComposant()
        {
        setLocation(300, 300);
        setTitle("Agence Ard€nnes");
        setSize(400, 600);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Bordure rouge pour texte et btn
        Border border = BorderFactory.createLineBorder(Color.red);

        // Panel ...
        panneau = new JPanel(new GridLayout(0, 1, 5, 5));
        panneau.setBorder(new EmptyBorder(10, 10, 10, 10));
        // zone texte ...
        titre = new JLabel("<html><b>Agence Ard€nnes</b><html>");
        titre.setFont(new Font(Font.DIALOG, Font.PLAIN, 48));
        nom = new JLabel("<html><b>Nom du compte client</b><html>");
        nom.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
        mdp = new JLabel("<html><b>Mot de passe</b><html>");
        mdp.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
        zoneText = new JTextField("", 25);
        zoneTextMdp = new JTextField("", 25);
        zoneText.setPreferredSize(new Dimension(90, 40));
        zoneText.setForeground(Color.BLACK);
        zoneText.setFont(new Font("Courier", Font.BOLD, 15));
        zoneTextMdp.setForeground(Color.BLACK);
        zoneTextMdp.setFont(new Font("Courier", Font.BOLD, 15));
        zoneTextMdp.setPreferredSize(new Dimension(40, 40));
        //zoneText.setBorder(border);
        // bouton ...
        setCadreDeux = new JButton("<html><b>Ok !</b><html>");

        //Classe anonyme pour l'ajout du listener ...
        setCadreDeux.addActionListener(new ActionListener()
            {
            @Override
            public void actionPerformed(ActionEvent e)
                {
                    if(!zoneText.getText().equals("") || !zoneTextMdp.getText().equals("")){
                        String requete ="SELECT NOM,PRENOM,COMPTE_NUM,SOLDE FROM CLIENT,DETENIR,COMPTE,CONNEXION,AUTHENTIFIER WHERE CLIENT.ID_CLIENT=DETENIR.ID_CLIENT AND DETENIR.ID_COMPTE= COMPTE.ID_COMPTE AND CONNEXION.ID_CONNEXION=AUTHENTIFIER.ID_CONNEXION AND CLIENT.ID_CLIENT=AUTHENTIFIER.ID_CLIENT AND CLIENT.NOM='"+zoneText.getText()+"' AND MOTDEPASSE = MD5('"+zoneTextMdp.getText()+"')";
                        var DB = new BDD(requete);
                        try{
                            DB.EXECUTION();
                        }catch(Exception a){
                            System.out.println("Erreur !");
                            System.out.println(a);
                        }
                        fenetreDeux.setNom(DB.Nom());
                        fenetreDeux.setPrenom(DB.Prenom());
                        fenetreDeux.setCompte(DB.Compte(),DB.Solde());
                        fenetreDeux.setVisible(true);
                        FEN_UNE.this.setVisible(false);
                    }
                }
            });
        setCadreDeux.setBorder(border);
        setCadreDeux.setPreferredSize(new Dimension(10, 12));
        
        panneau.add(titre);
        panneau.add(nom);
        panneau.add(zoneText);
        panneau.add(mdp);
        panneau.add(zoneTextMdp);
        panneau.add(setCadreDeux);

        add(panneau, BorderLayout.CENTER);
        }

    /**
     * Affectation de l'obj correspondant à la 2ème fen.
     *
     * @param f2
     */
    public void setFenDeux(FEN_DEUX f2)
        {
        this.fenetreDeux = f2;
        }

    /**
     *
     * @param txt
     */
    public void setText(String txt)
        {
        this.zoneText.setText(txt);
        }
    }
