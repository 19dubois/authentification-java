/*
 * Copyright (C) 2020 Marcellin DUBOIS <marcellin.dubois@orange.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package sio2_auth_banc;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Properties;
/**
 *
 * @author Marcellin DUBOIS <marcellin.dubois@orange.fr>
 */
public class BDD {
    String requete = "";
    String nom = "";
    String prenom = "";
    String nb_Colonne ="" ;
    int nombreColonne = 0;
    String compte[] = {}; 
    String solde[] = {}; 
    public BDD(String args){
        this.requete = args;
    }
    /**
     * EXECUTION
     * 
     * Récupère les informations et la requête néccesaire à la connexion de la bdd et éxecute la requête puis assigne les réponses aux variables globales
     * 
     * @throws Exception
     */
    public void EXECUTION()throws Exception 
    {
        nom = "";
        prenom = "";
        nb_Colonne ="";
        //home/siostudent/Bureau/SIO2_AUTH_BANC_V1/src/sio2_auth_banc/CONFIG.prop
        FileInputStream fis=new FileInputStream("/home/siostudent/authentification-java/Authentification/src/sio2_auth_banc/CONFIG.prop");
        // Warning!
        Properties p=new Properties ();
        p.load(fis);
        String url      =(String) p.get("URL");
        String username =(String) p.get("LOGIN");
        String password =(String) p.get("MOTdePASSE");
        Connection con = DriverManager.getConnection(url, username, password);
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(this.requete);
        int y = -1;
        compte = new String[10];
        solde = new String[10];
        while (rs.next())
            {
            // 'infoCol' => Nom de la colonne (non iuilisé ici)
            ResultSetMetaData infoCol = rs.getMetaData();
            // Nb de lignes dans le résultat
            int numOfCols = infoCol.getColumnCount();
            // du 1er au dernier résultat
                y = y+1;
            for (int i = 1; i < numOfCols; i++)
                {
                // Récup. et affichage du résultat à l'indice 'i'
                nom = rs.getString(1);
                prenom = rs.getString(2);
                compte[0+y] = rs.getString(3);
                solde[0+y] = String.valueOf(rs.getString(4));
                }
            }
             rs.close();
        stmt.close();
        con.close();
    }
    /**
     * Nom
     * 
     * Retourne le nom du client
     * 
     * @return :string
     */
    public String Nom()
    {
        return nom;
    }
    /**
     * Prenom
     * 
     * Retourne le prenom du client
     * 
     * @return :string
     */
    public String Prenom()
    {
        return prenom;
    }
    /**
     * Compte
     * 
     * Retourne un tableau composer de chaînes textes avec la liste des comptes
     * 
     * @return: String[]
     */
    public String[] Compte()
    {
        return compte;
    }
    /**
     * Solde
     * 
     * Retourne un tableau composer de chaînes textes avec la quantité d'argent dans le compte
     * 
     * @return: String[]
     */
    public String[] Solde()
    {
        return solde;
    }
}
